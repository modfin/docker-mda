#!/usr/bin/env bash


mkdir /mda;

wget -O /mda/hashit.tgz "https://packages.millistream.com/source/libhashit-$HASHIT_VERSION.tar.gz"
tar -xvf /mda/hashit.tgz -C /mda
cd /mda/libhashit-$HASHIT_VERSION && cmake . && make && make install

wget -O /mda/mdf.tgz "https://packages.millistream.com/source/libmdf-$MDF_VERSION.tar.gz";
tar -xvf /mda/mdf.tgz -C /mda
cd /mda/libmdf-$MDF_VERSION && ./configure && make && make install

wget -O /mda/mda.tgz "https://packages.millistream.com/source/mda-$MDA_VERSION.tar.gz";
tar -xvf /mda/mda.tgz -C /mda
cd /mda/mda-$MDA_VERSION && make mod_postgresql.so && make mda && make install
