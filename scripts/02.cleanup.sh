#!/usr/bin/env bash

apk del bash \
		gawk \
		gcc \
		cmake \
		g++ \
		make;