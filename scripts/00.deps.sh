#!/bin/sh



apk update
apk add --update --no-cache \
		bash \
		gawk \
		gcc \
		cmake \
		g++ \
		make;

apk add --update --no-cache \
        zlib-dev \
        expat-dev \
        mysql-dev \
        postgresql-dev \
		libressl-dev;
