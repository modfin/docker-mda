FROM alpine:latest

ENV HASHIT_VERSION 0.9.7
ENV MDF_VERSION 1.0.22
ENV MDA_VERSION 0.9.44

COPY scripts /scripts

RUN /scripts/00.deps.sh && /scripts/01.install.sh && /scripts/02.cleanup.sh
WORKDIR /mda

CMD mda --console